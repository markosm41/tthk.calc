﻿using Calc.Core.Domain;

namespace Calc.ApplicationServices.DataStructures
{
    public class BinaryCalcNode : IBinaryCalcNode
    {
        public BinaryCalcNode(IBinaryOperator @operator, ICalcNode left, ICalcNode right)
        {
            Operator = @operator;
            Left = left;
            Right = right;
        }

        public decimal GetValue()
        {
            return Operator.Exec(Left.GetValue(), Right.GetValue());
        }

        public IBinaryOperator Operator { get; }
        public ICalcNode Left { get; set; }
        public ICalcNode Right { get; }
    }
}
