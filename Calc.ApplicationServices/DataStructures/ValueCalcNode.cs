﻿namespace Calc.ApplicationServices.DataStructures
{
    public class ValueCalcNode : IValueNode
    {
        public ValueCalcNode(decimal value)
        {
            Value = value;
        }

        public decimal GetValue()
        {
            return Value;
        }

        public decimal Value { get; }
    }
}
