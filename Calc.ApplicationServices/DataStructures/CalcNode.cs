﻿using Calc.Core.Domain;

namespace Calc.ApplicationServices.DataStructures
{

    public interface ICalcNode
    {
        decimal GetValue();
    }

    public interface IBinaryCalcNode : ICalcNode
    {
        IBinaryOperator Operator { get; }
        ICalcNode Left { get; }
        ICalcNode Right { get; }
    }

    public interface IUnaryCalcNode : ICalcNode
    {
        IUnaryOperator Operator { get; }
        ICalcNode Argument { get; }
    }

    public interface IValueNode : ICalcNode
    {
        decimal Value { get; }
    }
}
