﻿namespace Calc.Core.Domain
{
    public class ValueCalcNode : CalcNode
    {
        public ValueCalcNode(decimal value)
        {
            Value = value;
        }

        public override decimal GetValue()
        {
            return Value;
        }

        public override string GetString()
        {
            throw new System.NotImplementedException();
        }

        public decimal Value { get; }
    }

}
