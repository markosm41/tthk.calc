﻿namespace Calc.Core.Domain
{
    public interface IExpressionNode
    {

    }

    public interface IOperator : IExpressionNode
    {
        string Symbol { get; }
        int Priority { get; }

    }

    public interface IUnaryOperator : IOperator
    {
        decimal Exec(decimal val);

    }

    public interface IBinaryOperator : IOperator
    {
        decimal Exec(decimal left, decimal right);
    }

    public class Value : IExpressionNode
    {
        public string StringValue { get; set; }
        
        public decimal NumericValue { get; set; }
    }
}
